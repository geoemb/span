#!/usr/bin/python

from hyperparameters.tuner_optuna import OptunaTuner as ot
import sys


def main(argv):
    """

    """

    id_dataset = sys.argv[1]
    num_nearest = int(sys.argv[2])
    sequence = sys.argv[3]
    n_trials = int(sys.argv[4])


    ot(id_dataset=id_dataset, num_nearest=num_nearest, sequence=sequence, n_trials=n_trials)()


if __name__ == "__main__":
    main(sys.argv[1:])
