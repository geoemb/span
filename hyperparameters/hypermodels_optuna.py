from span.model import SpatialPredictionAttentionModel
from tensorflow.keras.backend import clear_session
import optuna
from loguru import logger
import time
import copy
from config import PATH
from train_repet import train_repet


class SpanHyperModel():
    """

    """

    def __init__(self, id_dataset, num_nearest, sequence, n_trials):

        self.id_dataset = id_dataset
        self.num_nearest = num_nearest
        self.sequence = sequence
        self.n_trials = n_trials

    def __call__(self, *args, **kwargs):
        """

        """
        """
        #
        height_train = copy.deepcopy(self.X_train.shape[0])
        cut = int(height_train * (1 - 0.2))
        train_x_d = copy.deepcopy(self.train_x_d)
        train_x_g = copy.deepcopy(self.train_x_g)
        train_x_p = copy.deepcopy(self.train_x_p)
        train_x_e = copy.deepcopy(self.train_x_e)
        train_x_co = copy.deepcopy(self.train_x_co)
        train_x_c = copy.deepcopy(self.train_x_c)
        train_x_co_90 = copy.deepcopy(self.train_x_co_90)
        train_x_c_90 = copy.deepcopy(self.train_x_c_90)
        X_train = copy.deepcopy(self.X_train)
        y_train = copy.deepcopy(self.y_train)

        if self.geo:
            self.train_x_d = train_x_d[:cut, :, :]
            self.test_x_d = train_x_d[cut:, :, :]
            self.train_x_g = train_x_g[:cut, :]
            self.test_x_g = train_x_g[cut:, :]
        if self.euclidean:
            self.train_x_p = train_x_p[:cut, :, :]
            self.test_x_p = train_x_p[cut:, :, :]
            self.train_x_e = train_x_e[:cut, :]
            self.test_x_e = train_x_e[cut:, :]
        if self.cosine:
            self.train_x_co = train_x_co[:cut, :, :]
            self.test_x_co = train_x_co[cut:, :, :]
            self.train_x_c = train_x_c[:cut, :]
            self.test_x_c = train_x_c[cut:, :]
            self.train_x_co_90 = train_x_co_90[:cut, :, :]
            self.test_x_co_90 = train_x_co_90[cut:, :, :]
            self.train_x_c_90 = train_x_c_90[:cut, :]
            self.test_x_c_90 = train_x_c_90[cut:, :]

        self.X_train = X_train[:cut, :]
        self.X_test = X_train[cut:, :]
        self.y_train = y_train[:cut]
        self.y_test = y_train[cut:]

        del train_x_d
        del train_x_g
        del train_x_p
        del train_x_e
        del train_x_co
        del train_x_c
        del train_x_co_90
        del train_x_c_90
        del X_train
        del y_train
        """
        return self.run_tuner()

    def build_tuner(self, trial):

        #
        validation_split = 0.1

        ######################## Hyperparameters ########################

        #
        geointerpolation = 'simple span'
        optimizer = 'adam'
        graph_label = 'matrix'
        label = 'hyperparameters_simple_span'
        geo = True

        #type_compat_funct_eucli = 'identity'  # trial.suggest_categorical('type_compat_funct_eucli', ['identity',
        # 'kernel_gaussiano'])

        type_compat_funct_geo = 'kernel_gaussiano'  # trial.suggest_categorical('type_compat_funct_geo', ['identity',
        # 'kernel_gaussiano'])



        if geo:
            if type_compat_funct_geo == 'kernel_gaussiano':
                #
                sigma_geo = trial.suggest_float("sigma_geo", 1, 100)
            else:
                sigma_geo = 0
        else:
            sigma_geo = 0
        """
        if self.euclidean:
            if type_compat_funct_eucli == 'kernel_gaussiano':
                #
                sigma_eucli = trial.suggest_float("sigma_eucli", 0.1, 30)
            else:
                sigma_eucli = 0
        else:
            sigma_eucli = 0
            
        """

        #
        attn_heads = trial.suggest_int("attn_heads", 1, 4, step=1)
        attn_heads_reduction = trial.suggest_categorical('attn_heads_reduction', ['concat', 'average'])
        activation = trial.suggest_categorical('activation', ['elu', 'relu', 'linear', 'sigmoid', 'tanh'])
        activation_att = trial.suggest_categorical('activation_att', ['elu', 'relu', 'linear', 'sigmoid', 'tanh'])
        activation_output = trial.suggest_categorical('activation_output', ['elu', 'relu', 'linear'])
        F_ = trial.suggest_int("Fg_", 5, 100, step=5)
        F__ = trial.suggest_int("Fg__", 5, 100, step=5)
        learning_rate = trial.suggest_loguniform("lr", 1e-5, 1e-1)
        num_layers = trial.suggest_int("num_layers", 0, 10,  step=1)
        num_neuron = trial.suggest_int("num_neuron", 10, 1000, step=10)
        size_embedded = trial.suggest_int("size_embedded", 50, 2000, step=50)
        batch_size = trial.suggest_int("batch_size", 50, 2000, step=50)
        nearest = trial.suggest_int("nearest", 2, 30, step=2)

        """
        if self.geo:
            num_nearest_geo = trial.suggest_int("num_nearest_geo", 1, self.num_nearest,  step=1)
        else:
            num_nearest_geo = 0
        
        if self.euclidean:
            num_nearest_eucli = trial.suggest_int("num_nearest_eucli", 1, self.num_nearest,  step=1)
        else:
            num_nearest_eucli = 0
        """

        # Clear clutter from previous Keras session graphs.
        clear_session()

        hyperparameter = {
            "num_nearest": self.num_nearest,
            "sigma_geo": sigma_geo,
            "sigma_eucli": 0,
            "learning_rate": learning_rate,
            "batch_size": batch_size,
            "num_neuron": num_neuron,
            "num_layers": num_layers,
            "size_embedded": size_embedded,
            "num_nearest_geo": nearest,
            "num_nearest_eucli": nearest,
            "id_dataset": self.id_dataset,
            "epochs": 300,
            "optimier": 'adam',
            "validation_split": 0.1,
            "label": label,
            "early_stopping": True,
            "graph_label": 'matrix',
            "attn_heads": attn_heads,
            "F_": F_,
            "F__": F__,
            "activation_att": activation_att,
            "attn_heads_reduction": attn_heads_reduction,
            "activation": activation,
            "activation_output": activation_output,
            "geo": True,
            "euclidean": True,
            "input_target_context": True,
            "input_dist_context_geo": False,
            "input_dist_context_eucl": False,
            "sequence": self.sequence
        }

        score = train_repet(hyperparameter, 3)

        """

        ################ build model ################

        model = self.build(geointerpolation=geointerpolation,
                           sigma=[0, sigma_geo],
                           learning_rate=learning_rate,
                           num_layers=num_layers,
                           num_neuron=num_neuron,
                           size_embedded=size_embedded,
                           graph_label=graph_label,
                           type_compat_funct_eucli=type_compat_funct_eucli,
                           type_compat_funct_geo=type_compat_funct_geo,
                           num_nearest_geo=nearest,
                           num_nearest_eucli=nearest,
                           attn_heads=attn_heads,
                           attn_heads_reduction=attn_heads_reduction,
                           activation=activation,
                           activation_output=activation_output,
                           F_=F_,
                           F__=F__)

        ################ Fit o model ################

        weights, _ = self.train(model=model, batch_size=batch_size, label=label, num_nearest_geo=nearest,
                             num_nearest_eucli=nearest)

        ################ Predict ################

        score = self.predict_value(model=model, weights=weights, batch_size=batch_size,
                                   num_nearest_geo=nearest, num_nearest_eucli=nearest)
        """

        return score[2]

    def run_tuner(self):

        label = self.id_dataset + '_nearest_' + str(self.num_nearest)

        logger.add(PATH + "/logs/hyperparameters/" + self.id_dataset + "/" + label + "_{time}.log")
        logger.info("Start execution")
        execution_start = time.time()

        study = optuna.create_study(direction="minimize")
        study.optimize(self.build_tuner, n_trials=self.n_trials)

        #
        trial = study.best_trial

        execution_stop = time.time()
        elapsed_time = execution_stop - execution_start
        logger.info(
            f"Elapsed time = {elapsed_time:10.4f} s, result = {trial}"
        )

        return trial.value, trial.params, study
