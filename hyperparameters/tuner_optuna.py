from hyperparameters.hypermodels_optuna import SpanHyperModel

class OptunaTuner:
    """

    """

    def __init__(self, id_dataset: str, num_nearest: int = None, n_trials: int = None, tuner: str = 'simple span',
                 sequence: str =''):

        self.id_dataset = id_dataset
        self.num_nearest = num_nearest
        self.sequence = sequence
        self.n_trials = n_trials
        self.tuner = tuner

        self.choices = {
            'simple span': self.run_hyperparameter_tuning_simple_span,
        }

    def __call__(self, *args, **kwargs):

        choice = self.tuner
        action = self.choices.get(choice)
        if action:

            hyperparameters = action()

        else:
            hyperparameters = "{0} is not a valid choice".format(choice)
            print(hyperparameters)

        return hyperparameters

    def run_hyperparameter_tuning_simple_span(self):
        trial = SpanHyperModel(id_dataset=self.id_dataset,
                               num_nearest=self.num_nearest,
                               sequence=self.sequence,
                               n_trials=self.n_trials)()

        return trial