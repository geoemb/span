from tensorflow.keras import backend as K, activations
from tensorflow.keras.layers import Layer, BatchNormalization, concatenate
import tensorflow as tf
from tensorflow.keras.layers import multiply, RepeatVector, LeakyReLU
from tensorflow.keras.layers import Lambda, Permute, Dropout
from span.distance import Distance
from span.transformation import CompFunction


class Attention(Layer):

    def __init__(self,
                 F_,
                 sigma,
                 num_nearest,
                 attn_heads,
                 type_compatibility_function,
                 attn_heads_reduction='concat',  # {'concat', 'average'}
                 calculate_distance=False,
                 graph_label=None,
                 phenomenon_structure_repeat=None,
                 context_structure=None,
                 type_distance=None,
                 suffix_mean=None,
                 last=False,
                 activation='relu',
                 dropout_rate=0.0001,
                 **kwargs):

        if attn_heads_reduction not in {'concat', 'average'}:
            raise ValueError('Possbile reduction methods: concat, average')

        self.sigma = sigma,
        self.num_nearest = num_nearest,
        self.type_compatibility_function = type_compatibility_function,
        self.calculate_distance = calculate_distance,
        self.graph_label = graph_label,
        self.phenomenon_structure_repeat = phenomenon_structure_repeat,
        self.context_structure = context_structure,
        self.type_distance = type_distance,
        self.suffix_mean = suffix_mean
        self.attn_heads = attn_heads
        self.attn_heads_reduction = attn_heads_reduction
        self.last = last
        self.activation = activations.get(activation)
        self.dropout_rate = dropout_rate

        # Populated by build()
        self.kernels = []  # Layer kernels for attention heads
        self.attn_kernels = []  # Attention kernels for attention heads
        self.distances = []  # Attention kernels for attention heads

        self.biases = []  # Layer biases for attention heads
        self.biases_kernels = []  # Layer biases for attention heads
        self.biases_distances = []  # Layer biases for attention heads

        self.F_ = F_

        if attn_heads_reduction == 'concat':
            # Output will have shape (..., K * F')
            self.output_dim = self.F_ * self.attn_heads
        else:
            # Output will have shape (..., F')
            self.output_dim = self.F_

        super(Attention, self).__init__(**kwargs)

    def build(self, input_shape):
        assert len(input_shape) >= 2

        S = input_shape[0][-1]
        self.F = input_shape[1][-1]


        # Initialize weights for each attention head
        for head in range(self.attn_heads):

            # Layer kernel
            kernel = self.add_weight(shape=(self.F, self.F_), name='kernel_{}'.format(head))
            self.kernels.append(kernel)

            # Attention kernels
            attn_kernel = self.add_weight(shape=(self.num_nearest[0]+1, self.num_nearest[0]+1),
                                          name='attn_kernel_{}'.format(head))

            self.attn_kernels.append(attn_kernel)

            #Layer bias
            bias = self.add_weight(shape=(self.num_nearest[0]+1,),
                                   name='bias_{}'.format(head))

            self.biases.append(bias)

        self.built = True

    def call(self, inputs):
        source_distance = inputs[0]  # Node features (K+1,K+1)
        context = inputs[1]     # Node features (K x F)

        outputs = []
        simis = []

        ######################## Attention data ########################

        for head in range(self.attn_heads):

            if self.calculate_distance[0]:

                dist = Distance(self.F_, self.type_distance[0])
                distance = dist.run()

            else:
                distance = source_distance

            # calculate the similarity measure of each neighbor (m, seq)
            distance = K.reshape(distance, [-1, (self.num_nearest[0]+1)**2])
            comp_func = CompFunction(self.sigma[0], distance, self.type_compatibility_function[0], self.graph_label[0])
            simi = comp_func.run()
            simi = K.reshape(simi, [-1, self.num_nearest[0]+1, self.num_nearest[0]+1])

            attn_kernel = self.attn_kernels[head]  # W in the paper (F x F')
            kernel = self.kernels[head]
            bias = self.biases[head]

            features = K.dot(context, kernel) # (N x F) . (F x F_)
            features = self.activation(features)

            # calculates the weights associated with each neighbor (m, seq)
            weight = K.dot(simi, attn_kernel) # (N x N) . (N x N)
            weight = K.bias_add(weight, bias)
            #weight = tf.reshape(weight, [-1, 1, weight.shape[-1]])

            if not self.last:
                weight = LeakyReLU(alpha=0.2)(weight)

            weight = K.softmax(weight)

            #dropout_simi = Dropout(self.dropout_rate)(simi)
            #dropout_weight = Dropout(self.dropout_rate)(weight)
            #dropout_features = Dropout(self.dropout_rate)(features)

            mean = K.batch_dot(weight, features) # (N x N) . (N x F_)
            #mean = tf.reshape(mean, [-1, self.F_])

            simis.append(weight)

            outputs.append(mean)

        #simi_output = K.mean(K.stack(simis), axis=0)

        # Aggregate the heads' output according to the reduction method
        if self.attn_heads_reduction == 'concat':
            output = K.concatenate(outputs)  # (N x KF')
            simi_output = K.concatenate(simis)
        else:
            output = K.mean(K.stack(outputs), axis=0)  # N x F')
            simi_output = K.mean(K.stack(simis), axis=0)

        if not self.last:
            output = self.activation(output)
            simi_output = self.activation(simi_output)

        return simi_output, output

    def compute_output_shape(self, input_shape):
        output_shape = input_shape[0][0], self.output_dim
        return output_shape
