from span.model import SpatialPredictionAttentionModel as span
from tensorflow.keras.backend import clear_session

class train:

    def __init__(self, sigma_geo, sigma_eucli, learning_rate, batch_size, num_neuron, num_layers, size_embedded,
                 num_nearest_geo, num_nearest_eucli, id_dataset, label, graph_label, num_nearest,
                 epochs, validation_split, early_stopping, optimier, F_, F__, attn_heads,
                 attn_heads_reduction, activation, activation_output, geo, euclidean, activation_att,
                 input_target_context, input_dist_context_geo, input_dist_context_eucl, sequence,
                 **kwargs):
        """

        :param sigma:
        :param learning_rate:
        :param batch_size:
        :param num_neuron:
        :param num_layers:
        :param size_embedded:
        :param num_nearest_geo:
        :param num_nearest_eucli:
        :param id_dataset:
        :param label:
        :param graph_label:
        :param num_nearest:
        :param epochs:
        :param validation_split:
        :param early_stopping:
        :param optimier:
        :param kwargs:
        """

        self.NUM_NEAREST = num_nearest
        self.SIGMA_GEO = sigma_geo
        self.SIGMA_EUCLI = sigma_eucli
        self.LEARNING_RATE = learning_rate
        self.BATCH_SIZE = batch_size
        self.NUM_NEURON = num_neuron
        self.NUM_LAYERS = num_layers
        self.SIZE_EMBEDDED = size_embedded
        self.NUM_NEAREST_GEO = num_nearest_geo
        self.NUM_NEAREST_EUCLI = num_nearest_eucli
        self.ID_DATASET = id_dataset
        self.EPOCHS = epochs
        self.OPTIMIZER = optimier
        self.VALIDATION_SPLIT = validation_split
        self.LABEL = label
        self.EARLY_STOPPING = early_stopping
        self.GRAPH_LABEL = graph_label
        self.ATTN_HEADS = attn_heads
        self.F_ = F_
        self.F__ = F__
        self.activation_att = activation_att
        self.attn_heads_reduction = attn_heads_reduction
        self.activation = activation
        self.activation_output = activation_output
        self.geo = geo
        self.euclidean = euclidean
        self.input_target_context = input_target_context
        self.input_dist_context_geo = input_dist_context_geo
        self.input_dist_context_eucl = input_dist_context_eucl
        self.sequence = sequence

    def __call__(self):
        ####################################### Model ##########################################

        clear_session()

        # build of the object
        spatial = span(id_dataset=self.ID_DATASET,
                       num_nearest=self.NUM_NEAREST,
                       early_stopping=self.EARLY_STOPPING,
                       geo=self.geo,
                       euclidean=self.euclidean,
                       sequence=self.sequence,
                       input_target_context=self.input_target_context,
                       input_dist_context_geo=self.input_dist_context_geo,
                       input_dist_context_eucl=self.input_dist_context_eucl
                       )

        # build of the model
        model = spatial.build(sigma=[self.SIGMA_EUCLI, self.SIGMA_GEO],
                              optimizer=self.OPTIMIZER,
                              learning_rate=self.LEARNING_RATE,
                              num_layers=self.NUM_LAYERS,
                              num_neuron=self.NUM_NEURON,
                              size_embedded=self.SIZE_EMBEDDED,
                              graph_label=self.GRAPH_LABEL,
                              num_nearest_geo=self.NUM_NEAREST_GEO,
                              num_nearest_eucli=self.NUM_NEAREST_EUCLI,
                              attn_heads=self.ATTN_HEADS,
                              F_=self.F_,
                              F__=self.F__,
                              activation_att=self.activation_att,
                              attn_heads_reduction=self.attn_heads_reduction,
                              activation=self.activation,
                              activation_output=self.activation_output
                              )

        # save architecture image
        spatial.architecture(model, 'architecture_' + self.LABEL)

        # fitt of the model
        weight, fit = spatial.train(model=model,
                                    epochs=self.EPOCHS,
                                    batch_size=self.BATCH_SIZE,
                                    validation_split=self.VALIDATION_SPLIT,
                                    label=self.LABEL,
                                    num_nearest_geo=self.NUM_NEAREST_GEO,
                                    num_nearest_eucli=self.NUM_NEAREST_EUCLI)

        # prediction
        result = spatial.predict_value(model=model,
                                       weights=weight,
                                       num_nearest_geo=self.NUM_NEAREST_GEO,
                                       num_nearest_eucli=self.NUM_NEAREST_EUCLI,
                                       batch_size=self.BATCH_SIZE)

        ############################ Feature Extraction ###########################################
        if self.geo and self.euclidean:
            DATA_TRAIN = ([spatial.X_train,
                           spatial.train_x_d[:, :self.NUM_NEAREST_GEO+1, :],
                           spatial.train_x_p[:, :self.NUM_NEAREST_EUCLI+1, :],
                           spatial.train_x_g[:, :self.NUM_NEAREST_GEO+1, :self.NUM_NEAREST_GEO+1],
                           spatial.train_x_e[:, :self.NUM_NEAREST_EUCLI+1, :self.NUM_NEAREST_EUCLI+1]]
            )

            DATA_TEST = ([spatial.X_test,
                          spatial.test_x_d[:, :self.NUM_NEAREST_GEO+1, :],
                          spatial.test_x_p[:, :self.NUM_NEAREST_EUCLI+1, :],
                          spatial.test_x_g[:, :self.NUM_NEAREST_GEO+1, :self.NUM_NEAREST_GEO+1],
                          spatial.test_x_e[:, :self.NUM_NEAREST_EUCLI+1, :self.NUM_NEAREST_EUCLI+1]]
            )
        elif self.geo and not self.euclidean:
            DATA_TRAIN = ([spatial.X_train,
                           spatial.train_x_d[:, :self.NUM_NEAREST_GEO+1, :],
                           spatial.train_x_g[:, :self.NUM_NEAREST_GEO+1, :self.NUM_NEAREST_GEO+1]]
            )

            DATA_TEST = ([spatial.X_test,
                          spatial.test_x_d[:, :self.NUM_NEAREST_GEO+1, :],
                          spatial.test_x_g[:, :self.NUM_NEAREST_GEO+1, :self.NUM_NEAREST_GEO+1]]
            )

        elif self.euclidean and not self.geo:
            DATA_TRAIN = ([spatial.X_train,
                           spatial.train_x_p[:, :self.NUM_NEAREST_EUCLI+1, :],
                           spatial.train_x_e[:, :self.NUM_NEAREST_EUCLI+1, :self.NUM_NEAREST_EUCLI+1]]
            )

            DATA_TEST = ([spatial.X_test,
                          spatial.test_x_p[:, :self.NUM_NEAREST_EUCLI+1, :],
                          spatial.test_x_e[:, :self.NUM_NEAREST_EUCLI+1, :self.NUM_NEAREST_EUCLI+1]]
            )
        else:
            DATA_TRAIN = ([spatial.X_train]
            )

            DATA_TEST = ([spatial.X_test]
            )

        # Embedded
        embedded_train = spatial.output_layer(model=model,
                                              weight=weight,
                                              layer='embedded',
                                              data=DATA_TRAIN,
                                              batch=self.BATCH_SIZE,
                                              file_name=self.ID_DATASET + '_embedded_train')
        embedded_test = spatial.output_layer(model=model,
                                             weight=weight,
                                             layer='embedded',
                                             data=DATA_TEST,
                                             batch=self.BATCH_SIZE,
                                             file_name=self.ID_DATASET + '_embedded_test')

        # Regression
        predict_regression_train = spatial.output_layer(model=model,
                                                        weight=weight,
                                                        layer='main_output',
                                                        data=DATA_TRAIN,
                                                        batch=self.BATCH_SIZE,
                                                        file_name=self.ID_DATASET + '_predict_regression_train')

        predict_regression_test = spatial.output_layer(model=model,
                                                       weight=weight,
                                                       layer='main_output',
                                                       data=DATA_TEST,
                                                       batch=self.BATCH_SIZE,
                                                       file_name=self.ID_DATASET + '_predict_regression_test')

        return spatial, result, fit, embedded_train, embedded_test, predict_regression_train, predict_regression_test
