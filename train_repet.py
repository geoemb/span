from train_model import train
import statistics

def train_repet(hyperparameter, repet):
    results = []
    for i in range(repet):
        print('################  ' + str(i) + '  ###############')
        spatial = train(**hyperparameter)

        dataset, \
        result, \
        fit, \
        embedded_train, \
        embedded_test, \
        predict_regression_train, \
        predict_regression_test = spatial()

        results.append(result[0])



    print('Min: ' + str(min(results)))
    print('Max: ' + str(max(results)))
    print('Mean: ' + str(statistics.mean(results)))
    print('Variance: ' + str(statistics.variance(results)))

    return min(results), max(results), statistics.mean(results), statistics.variance(results)
