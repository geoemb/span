import sys
import numpy as np
import utils.utilsgeo as ug
from scipy.spatial import distance
from utils.utilsgeo import recovery_intra_dist


def create_input(argv):

    """

    intra: if intra distance exist
    """

    locate_dataset = sys.argv[1]
    k = int(sys.argv[2])
    generate_original = int(sys.argv[3])
    generate_intra = int(sys.argv[4])
    generate_transductive = int(sys.argv[5])

    if generate_original == 1:

        X_train = np.load(locate_dataset + 'X_train.npy', allow_pickle=True)
        X_test = np.load(locate_dataset + 'X_test.npy', allow_pickle=True)
        y_train = np.load(locate_dataset + 'y_train.npy', allow_pickle=True)
        y_test = np.load(locate_dataset + 'y_test.npy', allow_pickle=True)

        idx, dist = ug.create_sequence(X_train, X_test, k, 0)
        idx_eucli, dist_eucli = ug.recovery_dist(idx[:X_train.shape[0], :],
                                                 idx[X_train.shape[0]:, :], X_train[:, 2:],
                                                 X_test[:, 2:], distance.euclidean)

        idx_geo = idx
        dist_geo = dist

        np.savez_compressed(locate_dataset + 'data.npz', dist_eucli=dist_eucli, dist_geo=dist_geo, idx_eucli=idx_eucli,
                            idx_geo=idx_geo, X_test=X_test, X_train=X_train, y_test=y_test, y_train=y_train)

        if generate_intra == 1:

            idx_geo_intra, dist_geo_intra = recovery_intra_dist(idx_geo, X_train, X_test)
            idx_eucli_intra, dist_eucli_intra = recovery_intra_dist(idx_eucli, X_train, X_test,
                                                                    func=distance.euclidean, geo=False)

            np.savez_compressed(locate_dataset + 'data_intra.npz', dist_eucli=dist_eucli_intra, dist_geo=dist_geo_intra,
                                idx_eucli=idx_eucli_intra, idx_geo=idx_geo_intra, X_test=X_test, X_train=X_train, y_test=y_test,
                                y_train=y_train)

        if generate_transductive == 1:

            idx, dist = ug.create_sequence(X_train, X_test, k, 1)
            idx_eucli, dist_eucli = ug.recovery_dist(idx[:X_train.shape[0], :],
                                                     idx[X_train.shape[0]:, :], X_train[:, 2:],
                                                     X_test[:, 2:], distance.euclidean)

            idx_geo = idx

            idx_geo_intra_transductive, dist_geo_intra_transductive = recovery_intra_dist(idx_geo, X_train, X_test)
            idx_eucli_intra_transductive, dist_eucli_intra_transductive = recovery_intra_dist(idx_eucli, X_train, X_test,
                                                                    func=distance.euclidean, geo=False)

            np.savez_compressed(locate_dataset + 'data_intra.npz', dist_eucli=dist_eucli_intra_transductive,
                                dist_geo=dist_geo_intra_transductive, idx_eucli=idx_eucli_intra_transductive,
                                idx_geo=idx_geo_intra_transductive, X_test=X_test, X_train=X_train, y_test=y_test,
                                y_train=y_train)


    else:

        dataset = np.load(locate_dataset + 'data.npz', allow_pickle=True)

        X_train = dataset['X_train']
        X_test = dataset['X_test']
        y_train = dataset['y_train']
        y_test = dataset['y_test']

        if generate_intra == 1:

            idx_geo = dataset['idx_geo']
            idx_eucli = dataset['idx_eucli']

            idx_geo_intra, dist_geo_intra = recovery_intra_dist(idx_geo, X_train, X_test)
            idx_eucli_intra, dist_eucli_intra = recovery_intra_dist(idx_eucli, X_train, X_test, func=distance.euclidean,
                                                                    geo=False)

            np.savez_compressed(locate_dataset + 'data_intra.npz', dist_eucli=dist_eucli_intra, dist_geo=dist_geo_intra,
                                idx_eucli=idx_eucli_intra, idx_geo=idx_geo_intra, X_test=X_test, X_train=X_train,
                                y_test=y_test,
                                y_train=y_train)

        if generate_transductive == 1:

            idx, dist = ug.create_sequence(X_train, X_test, k, 1)
            idx_eucli, dist_eucli = ug.recovery_dist(idx[:X_train.shape[0], :],
                                                     idx[X_train.shape[0]:, :], X_train[:, 2:],
                                                     X_test[:, 2:], distance.euclidean)

            idx_geo = idx

            idx_geo_intra_transductive, dist_geo_intra_transductive = recovery_intra_dist(idx_geo, X_train, X_test)
            idx_eucli_intra_transductive, dist_eucli_intra_transductive = recovery_intra_dist(idx_eucli, X_train, X_test,
                                                                    func=distance.euclidean, geo=False)

            np.savez_compressed(locate_dataset + 'data_intra_transductive.npz', dist_eucli=dist_eucli_intra_transductive,
                                dist_geo=dist_geo_intra_transductive, idx_eucli=idx_eucli_intra_transductive,
                                idx_geo=idx_geo_intra_transductive, X_test=X_test, X_train=X_train, y_test=y_test,
                                y_train=y_train)


if __name__ == '__main__':
    create_input(sys.argv[1:])
